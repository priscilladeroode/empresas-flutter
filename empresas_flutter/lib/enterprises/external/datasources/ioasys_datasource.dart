import 'package:dio/native_imp.dart';
import 'package:empresas_flutter/enterprises/domain/failures/enterprise_failures.dart';
import 'package:empresas_flutter/enterprises/external/utils/enterprise_server_path.dart';
import 'package:empresas_flutter/enterprises/infrastructure/datasources/enterprise_datasource.dart';
import 'package:empresas_flutter/enterprises/infrastructure/models/enterprise_model.dart';

class IoasysDataSource implements EnterpriseDatasource {
  final DioForNative _dio;

  IoasysDataSource(this._dio);

  @override
  Future<List<EnterpriseModel>> getEnterprises(
      {required String enterpriseName}) async {
    final List<EnterpriseModel> listEnterprise = [];

    final result = await _dio
        .get(enterprisePath, queryParameters: {"name": enterpriseName});
    if (result.statusCode == 200) {
      final enterprises = result.data;
      enterprises['enterprises'].forEach((e) {
        listEnterprise.add(EnterpriseModel(
          id: e['id'] ?? 0,
          enterpriseName: e['enterprise_name'] ?? '',
          photoUrl: e['photo'] ?? '',
          description: e['description'] ?? '',
        ));
      });
      return listEnterprise;
    }
    if (result.statusCode == 401) {
      throw NotAuthenticatedFailure(message: 'Faça o login novamente.');
    } else {
      throw DataSourceFailure(message: 'Ops, ocorreu um erro.');
    }
  }
}

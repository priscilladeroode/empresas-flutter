import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/enterprises/domain/entities/enterprises.dart';
import 'package:empresas_flutter/enterprises/domain/failures/enterprise_failures.dart';
import 'package:empresas_flutter/enterprises/domain/repositories/enterprise_repository.dart';

abstract class GetEnterprisesByName {
  Future<Either<EnterpriseFailure, List<Enterprise>>> call(
      {required String enterpriseName});
}

class GetEnterprisesByNameImpl implements GetEnterprisesByName {
  final EnterpriseRepository repository;

  GetEnterprisesByNameImpl(this.repository);

  @override
  Future<Either<EnterpriseFailure, List<Enterprise>>> call(
      {required String enterpriseName}) async {
    return await repository.getEnterprises(enterpriseName: enterpriseName);
  }
}

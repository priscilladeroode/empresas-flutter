abstract class EnterpriseFailure implements Exception {
  String? get message;
  int? get statusCode;
}

class DataSourceFailure extends EnterpriseFailure {
  final String? message;
  final int? statusCode;
  DataSourceFailure({this.message, this.statusCode});
}

class NotAuthenticatedFailure extends EnterpriseFailure {
  final String? message;
  final int? statusCode;
  NotAuthenticatedFailure({this.message, this.statusCode});
}

import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/enterprises/domain/entities/enterprises.dart';
import 'package:empresas_flutter/enterprises/domain/failures/enterprise_failures.dart';

abstract class EnterpriseRepository {
  Future<Either<EnterpriseFailure, List<Enterprise>>> getEnterprises(
      {required String enterpriseName});
}

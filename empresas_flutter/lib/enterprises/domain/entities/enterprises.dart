import 'package:equatable/equatable.dart';

class Enterprise extends Equatable {
  final int id;
  final String enterpriseName;
  final String description;
  final String photoUrl;

  Enterprise({
    required this.id,
    required this.enterpriseName,
    required this.description,
    required this.photoUrl,
  });

  @override
  List<Object?> get props => [id, enterpriseName, description, photoUrl];
}

// "email_enterprise": null,
//             "facebook": null,
//             "twitter": null,
//             "linkedin": null,
//             "phone": null,
//             "own_enterprise": false,
//             "enterprise_name": "Fluoretiq Limited",
//             "photo": "/uploads/enterprise/photo/1/240.jpeg",
//             "description": "FluoretiQ is a Bristol based medtech start-up developing diagnostic technology to enable bacteria identification within the average consultation window, so that patients can get the right anti-biotics from the start.  ",
//             "city": "Bristol",
//             "country": "UK",
//             "value": 0,
//             "share_price": 5000.0,
//             "enterprise_type": {
//                 "id": 3,
//                 "enterprise_type_name": "Health"
//             }

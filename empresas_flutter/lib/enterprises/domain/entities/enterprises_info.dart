abstract class EnterpriseInfo {
  int get id;
  String get enterpriseName;
  String get description;
  String get photoUrl;
}

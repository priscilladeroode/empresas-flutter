import 'package:empresas_flutter/app_module.dart';
import 'package:empresas_flutter/enterprises/domain/usecases/get_enterprise.dart';
import 'package:empresas_flutter/enterprises/external/datasources/ioasys_datasource.dart';
import 'package:empresas_flutter/enterprises/infrastructure/repositories/enterprise_repository_impl.dart';
import 'package:empresas_flutter/enterprises/presenter/enterprise_details_screen.dart';
import 'package:empresas_flutter/enterprises/presenter/home_controller.dart';
import 'package:empresas_flutter/enterprises/presenter/home_screen.dart';
import 'package:flutter_modular/flutter_modular.dart';

class EnterpriseModule extends Module {
  @override
  final List<Bind> binds = [
    ...AppModule.exports,
    Bind.lazySingleton((i) => HomeController(i())),
    Bind.lazySingleton((i) => GetEnterprisesByNameImpl(i())),
    Bind.lazySingleton((i) => EnterpriseRepositoryImpl(i())),
    Bind.lazySingleton((i) => IoasysDataSource(i())),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, __) => Home()),
    ChildRoute('enterprise-details',
        child: (_, args) => EnterpriseDetailsScreen(enterprise: args.data)),
  ];
}

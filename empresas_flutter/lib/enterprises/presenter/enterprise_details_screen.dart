import 'package:empresas_flutter/core/themes/app_colors.dart';
import 'package:empresas_flutter/enterprises/domain/entities/enterprises.dart';
import 'package:empresas_flutter/enterprises/presenter/widgets/enterprise_banner.dart';
import 'package:flutter/material.dart';

class EnterpriseDetailsScreen extends StatelessWidget {
  final Enterprise enterprise;
  const EnterpriseDetailsScreen({
    Key? key,
    required this.enterprise,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 92,
              width: double.infinity,
              padding: const EdgeInsets.only(top: 36, bottom: 16, left: 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                      color: AppColors.wildSand,
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: BackButton(
                      color: AppColors.ceriseRed,
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Text(
                        enterprise.enterpriseName,
                        style: Theme.of(context).textTheme.headline5!.copyWith(
                              color: AppColors.pureBlack,
                              fontWeight: FontWeight.w500,
                            ),
                      ),
                    ),
                  ),
                  SizedBox(width: 40),
                ],
              ),
            ),
            Hero(
              tag: enterprise.id,
              child: EnterpriseBanner(enterprise: enterprise),
            ),
            Container(
              padding: const EdgeInsets.symmetric(
                vertical: 24,
                horizontal: 16,
              ),
              child: Text(enterprise.description,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontSize: 18,
                      )),
            ),
          ],
        ),
      ),
    );
  }
}

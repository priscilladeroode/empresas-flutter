import 'package:empresas_flutter/enterprises/domain/entities/enterprises.dart';
import 'package:empresas_flutter/enterprises/domain/usecases/get_enterprise.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final GetEnterprisesByName _getEnterprisesByName;

  _HomeControllerBase(this._getEnterprisesByName) {
    getEnterprises();
  }

  @observable
  ObservableList<Enterprise> enterprises = ObservableList<Enterprise>();

  @observable
  bool isLoading = false;

  @action
  setIsLoading() => isLoading = !isLoading;

  @observable
  String enterpriseFilter = '';

  @action
  setFilter(String enterpriseName) => enterpriseFilter = enterpriseName;

  @action
  setEnterprises({required List<Enterprise> newListEnterprises}) =>
      enterprises = ObservableList.of(newListEnterprises);

  Future<void> getEnterprises() async {
    setIsLoading();
    final result =
        await _getEnterprisesByName(enterpriseName: enterpriseFilter);
    result.fold(
      (failure) {
        asuka.showSnackBar(SnackBar(content: Text(failure.message!)));
        if (failure.statusCode == 401) {
          Modular.to.navigate('/auth', replaceAll: true);
        }
      },
      (newEnterprises) => setEnterprises(newListEnterprises: newEnterprises),
    );
    setIsLoading();
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$enterprisesAtom = Atom(name: '_HomeControllerBase.enterprises');

  @override
  ObservableList<Enterprise> get enterprises {
    _$enterprisesAtom.reportRead();
    return super.enterprises;
  }

  @override
  set enterprises(ObservableList<Enterprise> value) {
    _$enterprisesAtom.reportWrite(value, super.enterprises, () {
      super.enterprises = value;
    });
  }

  final _$isLoadingAtom = Atom(name: '_HomeControllerBase.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$enterpriseFilterAtom =
      Atom(name: '_HomeControllerBase.enterpriseFilter');

  @override
  String get enterpriseFilter {
    _$enterpriseFilterAtom.reportRead();
    return super.enterpriseFilter;
  }

  @override
  set enterpriseFilter(String value) {
    _$enterpriseFilterAtom.reportWrite(value, super.enterpriseFilter, () {
      super.enterpriseFilter = value;
    });
  }

  final _$_HomeControllerBaseActionController =
      ActionController(name: '_HomeControllerBase');

  @override
  dynamic setIsLoading() {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.setIsLoading');
    try {
      return super.setIsLoading();
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setFilter(String enterpriseName) {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.setFilter');
    try {
      return super.setFilter(enterpriseName);
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setEnterprises({required List<Enterprise> newListEnterprises}) {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.setEnterprises');
    try {
      return super.setEnterprises(newListEnterprises: newListEnterprises);
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
enterprises: ${enterprises},
isLoading: ${isLoading},
enterpriseFilter: ${enterpriseFilter}
    ''';
  }
}

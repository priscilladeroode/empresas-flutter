import 'package:empresas_flutter/enterprises/domain/entities/enterprises.dart';
import 'package:empresas_flutter/enterprises/presenter/widgets/enterprise_banner.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class EnterpriseCard extends StatelessWidget {
  final Enterprise enterprise;

  const EnterpriseCard({
    Key? key,
    required this.enterprise,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () =>
          Modular.to.navigate('enterprise-details', arguments: enterprise),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
        child: Hero(
          tag: enterprise.id,
          child: EnterpriseBanner(enterprise: enterprise),
        ),
      ),
    );
  }
}

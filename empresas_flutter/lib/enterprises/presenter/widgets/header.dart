import 'package:empresas_flutter/core/components/app_text_field_default.dart';
import 'package:empresas_flutter/core/helpers/assets_names.dart';
import 'package:flutter/material.dart';

class Header extends StatefulWidget {
  final Function(String)? onFilterChanged;

  Header({
    Key? key,
    this.onFilterChanged,
  }) : super(key: key);

  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  bool isExpanded = true;
  late FocusNode focusNode;

  @override
  void initState() {
    focusNode = FocusNode();
    focusNode.addListener(() {
      setState(() {
        isExpanded = !focusNode.hasFocus;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        AnimatedContainer(
          duration: Duration(milliseconds: 250),
          height: isExpanded ? 212 : 91,
          width: double.infinity,
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 250),
          height: isExpanded ? 188 : 67,
          width: double.infinity,
          margin: const EdgeInsets.only(bottom: 24),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AssetsNames.gradientBackgroudRectangle),
              fit: BoxFit.cover,
            ),
          ),
          child: Visibility(
            visible: isExpanded,
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(AssetsNames.iconsBackground),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ),
        Container(
          height: 48,
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: AppTextField(
            onTap: () {
              isExpanded = !isExpanded;
              setState(() {});
            },
            focusNode: focusNode,
            onChanged: widget.onFilterChanged,
            hintText: 'Pesquise por empresa',
            prefixIcon: Icon(Icons.search),
          ),
        ),
      ],
    );
  }
}

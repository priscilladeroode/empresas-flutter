import 'package:cached_network_image/cached_network_image.dart';
import 'package:empresas_flutter/enterprises/domain/entities/enterprises.dart';
import 'package:flutter/material.dart';

class EnterpriseBanner extends StatelessWidget {
  const EnterpriseBanner({
    Key? key,
    required this.enterprise,
  }) : super(key: key);

  final Enterprise enterprise;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: 'https://empresas.ioasys.com.br/' + enterprise.photoUrl,
      imageBuilder: (context, imgProvider) => Container(
        height: 120,
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          image: DecorationImage(
            image: imgProvider,
            fit: BoxFit.fitWidth,
          ),
        ),
        child: Center(
          child: Text(
            enterprise.enterpriseName.toUpperCase(),
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
      ),
    );
  }
}

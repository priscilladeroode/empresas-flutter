import 'package:empresas_flutter/core/components/loading_animation.dart';
import 'package:empresas_flutter/core/themes/app_colors.dart';
import 'package:empresas_flutter/enterprises/presenter/widgets/header.dart';
import 'package:empresas_flutter/enterprises/presenter/home_controller.dart';
import 'package:empresas_flutter/enterprises/presenter/widgets/enterprise_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends ModularState<Home, HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Header(
            onFilterChanged: (enterpriseName) {
              controller.setFilter(enterpriseName);
              controller.getEnterprises();
            },
          ),
          Observer(
            builder: (context) => Align(
              alignment: Alignment.centerLeft,
              child: Visibility(
                visible: controller.enterprises.length > 0,
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                      '${controller.enterprises.length} resultados encontrados'),
                ),
              ),
            ),
          ),
          Observer(builder: (_) {
            if (controller.isLoading) {
              return Expanded(child: LoadingAnimation());
            }
            if (controller.enterprises.isEmpty) {
              return Expanded(
                child: Center(
                  child: Text(
                    'Nenhum resultado encontrado',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontSize: 18,
                          color: AppColors.doveGrey,
                        ),
                  ),
                ),
              );
            } else {
              return Expanded(
                child: ListView.builder(
                  padding: const EdgeInsets.all(0),
                  shrinkWrap: true,
                  itemCount: controller.enterprises.length,
                  itemBuilder: (context, i) => EnterpriseCard(
                    enterprise: controller.enterprises[i],
                  ),
                ),
              );
            }
          }),
        ],
      ),
    );
  }
}

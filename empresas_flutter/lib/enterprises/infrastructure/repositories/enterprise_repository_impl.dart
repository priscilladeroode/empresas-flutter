import 'package:dio/dio.dart';
import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/enterprises/domain/entities/enterprises.dart';
import 'package:empresas_flutter/enterprises/domain/failures/enterprise_failures.dart';
import 'package:empresas_flutter/enterprises/domain/repositories/enterprise_repository.dart';
import 'package:empresas_flutter/enterprises/infrastructure/datasources/enterprise_datasource.dart';

class EnterpriseRepositoryImpl implements EnterpriseRepository {
  final EnterpriseDatasource datasource;

  EnterpriseRepositoryImpl(this.datasource);

  @override
  Future<Either<EnterpriseFailure, List<Enterprise>>> getEnterprises(
      {required String enterpriseName}) async {
    try {
      var enterprises =
          await datasource.getEnterprises(enterpriseName: enterpriseName);
      return Right(enterprises);
    } on DioError catch (e) {
      if (e.response?.statusCode == 401) {
        return Left(NotAuthenticatedFailure(
          message: 'Faça seu login novamente.',
          statusCode: 401,
        ));
      } else {
        return Left(DataSourceFailure(message: 'Ops, ocorreu um erro.'));
      }
    }
  }
}

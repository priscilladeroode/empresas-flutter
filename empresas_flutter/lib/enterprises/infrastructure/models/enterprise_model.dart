import 'package:empresas_flutter/enterprises/domain/entities/enterprises.dart';
import 'package:empresas_flutter/enterprises/domain/entities/enterprises_info.dart';

class EnterpriseModel extends Enterprise implements EnterpriseInfo {
  EnterpriseModel({
    required int id,
    required String enterpriseName,
    required String description,
    required String photoUrl,
  }) : super(
          id: id,
          enterpriseName: enterpriseName,
          description: description,
          photoUrl: photoUrl,
        );

  Enterprise toEnterprise() => this;
}

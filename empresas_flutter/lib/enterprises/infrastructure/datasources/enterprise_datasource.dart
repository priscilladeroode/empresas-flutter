import 'package:empresas_flutter/enterprises/infrastructure/models/enterprise_model.dart';

abstract class EnterpriseDatasource {
  Future<List<EnterpriseModel>> getEnterprises(
      {required String enterpriseName});
}

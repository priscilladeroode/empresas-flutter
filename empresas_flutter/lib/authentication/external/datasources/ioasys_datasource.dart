import 'package:dio/native_imp.dart';
import 'package:empresas_flutter/authentication/domain/failures/authentication_failures.dart';
import 'package:empresas_flutter/authentication/infrastructure/datasources/authentication_datasource.dart';
import 'package:empresas_flutter/authentication/infrastructure/models/user_model.dart';

class IoasysDataSource implements AuthenticationDatasource {
  final DioForNative _dio;

  IoasysDataSource(this._dio);

  @override
  Future<UserModel> loginWithEmail(
      {required String email, required String password}) async {
    final result = await _dio.post('/users/auth/sign_in',
        queryParameters: {"email": email, "password": password});
    if (result.statusCode == 200) {
      final user = result.headers;
      return UserModel(
        uid: user['uid']![0],
        accessToken: user['access-token']![0],
        client: user['client']![0],
      );
    } else {
      throw DataSourceFailure();
    }
  }
}

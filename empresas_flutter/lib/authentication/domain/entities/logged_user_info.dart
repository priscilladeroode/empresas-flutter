abstract class LoggedUserInfo {
  String get accessToken;
  String get uid;
  String get client;
}

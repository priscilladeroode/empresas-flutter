import 'package:equatable/equatable.dart';

class LoggedUser extends Equatable {
  final String accessToken;
  final String uid;
  final String client;

  LoggedUser({
    required this.accessToken,
    required this.uid,
    required this.client,
  });

  @override
  List<Object?> get props => [accessToken, uid, client];
}

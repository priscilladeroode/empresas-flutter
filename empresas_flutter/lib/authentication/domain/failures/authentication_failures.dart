abstract class AuthenticationFailure implements Exception {
  String? get message;
}

class InvalidCredentials extends AuthenticationFailure {
  final String message;
  InvalidCredentials({this.message = 'Credenciais inválidas'});
}

class ErrorLoginEmail extends AuthenticationFailure {
  final String? message;
  ErrorLoginEmail({this.message});
}

class ErrorLoginPassword extends AuthenticationFailure {
  final String? message;
  ErrorLoginPassword({this.message});
}

class DataSourceFailure extends AuthenticationFailure {
  final String? message;
  DataSourceFailure({this.message});
}

import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/authentication/domain/entities/logged_user_info.dart';
import 'package:empresas_flutter/authentication/domain/failures/authentication_failures.dart';

abstract class AuthenticationRepository {
  Future<Either<AuthenticationFailure, LoggedUserInfo>> loginWithEmail(
      {required String email, required String password});
}

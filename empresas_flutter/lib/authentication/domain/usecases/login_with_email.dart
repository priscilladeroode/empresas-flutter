import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/authentication/domain/entities/logged_user_info.dart';
import 'package:empresas_flutter/authentication/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/authentication/domain/failures/authentication_failures.dart';
import 'package:empresas_flutter/authentication/domain/repositories/authentication_repository.dart';

abstract class LoginWithEmail {
  Future<Either<AuthenticationFailure, LoggedUserInfo>> call(
      LoginCredentials credentials);
}

class LoginWithEmailImpl implements LoginWithEmail {
  final AuthenticationRepository repository;

  LoginWithEmailImpl(this.repository);

  @override
  Future<Either<AuthenticationFailure, LoggedUserInfo>> call(
      LoginCredentials credentials) async {
    if (!credentials.isValidEmail) {
      return Left(InvalidCredentials());
    }
    if (!credentials.isValidPassword) {
      return Left(InvalidCredentials());
    }

    return await repository.loginWithEmail(
      email: credentials.email,
      password: credentials.password,
    );
  }
}

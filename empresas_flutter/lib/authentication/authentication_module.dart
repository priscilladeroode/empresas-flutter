import 'package:empresas_flutter/app_module.dart';
import 'package:empresas_flutter/authentication/domain/usecases/login_with_email.dart';
import 'package:empresas_flutter/authentication/external/datasources/ioasys_datasource.dart';
import 'package:empresas_flutter/authentication/infrastructure/repositories/authentication_repository_impl.dart';
import 'package:empresas_flutter/authentication/presenter/sign_in_controller.dart';
import 'package:empresas_flutter/authentication/presenter/sign_in_screen.dart';
import 'package:empresas_flutter/core/components/loading_dialog.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AuthenticationModule extends Module {
  @override
  final List<Bind> binds = [
    ...AppModule.exports,
    Bind.lazySingleton((i) => SignInController(i(), i(), i())),
    Bind.lazySingleton((i) => LoginWithEmailImpl(i())),
    Bind.lazySingleton((i) => LoadingDialogImpl()),
    Bind.lazySingleton((i) => AuthenticationRepositoryImpl(i())),
    Bind.lazySingleton((i) => IoasysDataSource(i())),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, __) => SignInScreen()),
  ];
}

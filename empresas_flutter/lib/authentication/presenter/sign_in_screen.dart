import 'package:empresas_flutter/authentication/presenter/sign_in_controller.dart';
import 'package:empresas_flutter/authentication/presenter/widgets/header.dart';
import 'package:empresas_flutter/authentication/presenter/widgets/sign_in_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends ModularState<SignInScreen, SignInController> {
  bool isExpanded = true;

  FocusScopeNode? currentFocus;

  void clipHeader() {
    isExpanded = false;
    setState(() {});
  }

  void unclipHeader() {
    if (!currentFocus!.hasPrimaryFocus) {
      currentFocus!.unfocus();
      isExpanded = true;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    currentFocus = FocusScope.of(context);
    return Scaffold(
      body: GestureDetector(
        onTap: unclipHeader,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Header(
                isExpanded: isExpanded,
              ),
              Observer(
                builder: (context) => SignInForm(
                  errorMessage: controller.errorMessage,
                  onEmailFieldTap: clipHeader,
                  onEmailChanged: (value) =>
                      controller.setEmail(newEmail: value),
                  onPasswordFieldTap: clipHeader,
                  onPasswordChanged: (value) =>
                      controller.setPassword(newPassword: value),
                  onButtonPressed: () {
                    unclipHeader();
                    controller.enterEmail();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

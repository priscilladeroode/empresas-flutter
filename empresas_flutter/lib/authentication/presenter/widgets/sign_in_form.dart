import 'package:empresas_flutter/core/components/app_text_field_default.dart';
import 'package:empresas_flutter/core/themes/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SignInForm extends StatelessWidget {
  final Function()? onEmailFieldTap;
  final Function()? onPasswordFieldTap;
  final Function(String)? onEmailChanged;
  final Function(String)? onPasswordChanged;
  final Function()? onButtonPressed;
  final String errorMessage;

  const SignInForm({
    Key? key,
    this.onEmailFieldTap,
    this.onPasswordFieldTap,
    this.onEmailChanged,
    this.onPasswordChanged,
    this.onButtonPressed,
    this.errorMessage = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 16, right: 16, top: 100),
      child: Column(
        children: [
          AppTextField(
            errorMessage: errorMessage,
            label: 'Email',
            onTap: onEmailFieldTap,
            onChanged: onEmailChanged,
          ),
          SizedBox(height: 16),
          AppTextField(
            errorMessage: errorMessage,
            isPasswordField: true,
            label: 'Senha',
            onTap: onPasswordFieldTap,
            onChanged: onPasswordChanged,
          ),
          SizedBox(height: 4),
          Visibility(
            visible: errorMessage.isNotEmpty,
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(
                errorMessage,
                style: GoogleFonts.rubik(
                  fontSize: 12,
                  height: 1.3,
                  fontWeight: FontWeight.w300,
                  color: AppColors.errorRed,
                ),
              ),
            ),
          ),
          SizedBox(height: 40),
          Container(
            height: 48,
            width: double.infinity,
            child: ElevatedButton(
              onPressed: onButtonPressed,
              child: Text('ENTRAR'),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:empresas_flutter/core/helpers/assets_names.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final bool isExpanded;
  const Header({Key? key, this.isExpanded = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 250),
      height: isExpanded ? 233 : 136,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage(AssetsNames.gradientBackgroundSmall),
            fit: BoxFit.fill),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(AssetsNames.logoIcon),
          Visibility(
            visible: isExpanded,
            child: Column(
              children: [
                SizedBox(height: 16),
                Text(
                  'Seja bem vindo ao empresas!',
                  style: Theme.of(context).textTheme.headline5,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

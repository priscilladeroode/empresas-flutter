import 'package:empresas_flutter/authentication/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/authentication/domain/usecases/login_with_email.dart';
import 'package:empresas_flutter/core/components/loading_dialog.dart';
import 'package:empresas_flutter/core/presenter/auth_store.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'sign_in_controller.g.dart';

class SignInController = _SignInControllerBase with _$SignInController;

abstract class _SignInControllerBase with Store {
  final LoginWithEmail loginWithEmail;
  final LoadingDialog loading;
  final AuthStore authStore;

  _SignInControllerBase(this.loginWithEmail, this.loading, this.authStore);

  @observable
  String email = '';

  @action
  setEmail({required String newEmail}) {
    this.email = newEmail;
    setErrorMessage(newErrorMessage: '');
  }

  @observable
  String password = '';

  @action
  setPassword({required newPassword}) {
    this.password = newPassword;
    setErrorMessage(newErrorMessage: '');
  }

  @observable
  String errorMessage = '';

  @action
  setErrorMessage({required String newErrorMessage}) =>
      this.errorMessage = newErrorMessage;

  @computed
  LoginCredentials get credentials =>
      LoginCredentials.withEmailAndPassword(email: email, password: password);

  enterEmail() async {
    loading.show();
    await Future.delayed(Duration(seconds: 1));
    var result = await loginWithEmail(credentials);
    await loading.hide();
    result.fold((failure) {
      setErrorMessage(newErrorMessage: failure.message!);
    }, (user) {
      authStore.setUser(loggedUser: user);
      Modular.to.navigate('/enterprises', replaceAll: true);
    });
  }
}

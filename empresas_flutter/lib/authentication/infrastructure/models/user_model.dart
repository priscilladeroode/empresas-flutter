import 'package:empresas_flutter/authentication/domain/entities/logged_user.dart';
import 'package:empresas_flutter/authentication/domain/entities/logged_user_info.dart';

class UserModel extends LoggedUser implements LoggedUserInfo {
  UserModel({
    required String accessToken,
    required String uid,
    required String client,
  }) : super(
          accessToken: accessToken,
          uid: uid,
          client: client,
        );

  LoggedUser toLoggedUser() => this;
}

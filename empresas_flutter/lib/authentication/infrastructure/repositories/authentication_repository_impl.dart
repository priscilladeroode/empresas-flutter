import 'package:dio/dio.dart';
import 'package:empresas_flutter/authentication/domain/failures/authentication_failures.dart';
import 'package:empresas_flutter/authentication/domain/entities/logged_user_info.dart';
import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/authentication/domain/repositories/authentication_repository.dart';
import 'package:empresas_flutter/authentication/infrastructure/datasources/authentication_datasource.dart';

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  final AuthenticationDatasource datasource;

  AuthenticationRepositoryImpl(this.datasource);

  @override
  Future<Either<AuthenticationFailure, LoggedUserInfo>> loginWithEmail(
      {required String email, required String password}) async {
    try {
      var user =
          await datasource.loginWithEmail(email: email, password: password);
      return Right(user);
    } on DioError catch (e) {
      if (e.response?.statusCode == 401) {
        return Left(InvalidCredentials(message: 'Credenciais incorretas'));
      } else {
        return Left(DataSourceFailure(
            message: 'Ocorreu um erro. Tente novamente mais tarde.'));
      }
    }
  }
}

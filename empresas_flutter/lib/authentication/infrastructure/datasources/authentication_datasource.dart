import 'package:empresas_flutter/authentication/infrastructure/models/user_model.dart';

abstract class AuthenticationDatasource {
  Future<UserModel> loginWithEmail(
      {required String email, required String password});
}

import 'package:empresas_flutter/core/themes/app_colors.dart';
import 'package:empresas_flutter/core/themes/app_text_styles.dart';
import 'package:flutter/material.dart';

final ThemeData lightTheme = ThemeData(
  primarySwatch: AppColors.primarySwatch,
  textTheme: AppTextStyles.getTexttheme(),
  inputDecorationTheme: InputDecorationTheme(
    filled: true,
    fillColor: AppColors.wildSand,
    focusedBorder: UnderlineInputBorder(
      borderRadius: BorderRadius.circular(4),
      borderSide: BorderSide(
        color: Colors.transparent,
      ),
    ),
    enabledBorder: UnderlineInputBorder(
      borderRadius: BorderRadius.circular(4),
      borderSide: BorderSide(
        color: Colors.transparent,
      ),
    ),
  ),
  scaffoldBackgroundColor: AppColors.pureWhite,
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      elevation: 0,
      primary: AppColors.ceriseRed,
    ),
  ),
);

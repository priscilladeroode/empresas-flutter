import 'package:empresas_flutter/core/themes/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTextStyles {
  AppTextStyles._();

  static TextStyle _headline5 = TextStyle(
    fontSize: 20,
    height: 1.2,
    fontWeight: FontWeight.w400,
    color: AppColors.pureWhite,
  );

  static TextStyle _headline6 = TextStyle(
    fontSize: 18,
    height: 1.3,
    fontWeight: FontWeight.w700,
    color: AppColors.pureWhite,
  );
  static TextStyle _buttonStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    height: 1.5,
    color: AppColors.pureWhite,
  );
  static TextStyle _bodyText1 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w300,
    height: 1.5,
    color: AppColors.pureBlack,
  );

  static TextStyle _bodyText2 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    height: 1.3,
    color: AppColors.doveGrey,
  );

  static getTexttheme() => GoogleFonts.rubikTextTheme(
        TextTheme(
          headline6: _headline6,
          headline5: _headline5,
          button: _buttonStyle,
          bodyText1: _bodyText1,
          bodyText2: _bodyText2,
        ),
      );
}

import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color ceriseRed = Color(0xFFE01E69);
  static const Color wildSand = Color(0xFFF5F5F5);
  static const Color pureWhite = Color(0xFFFFFFFF);
  static const Color doveGrey = Color(0xFF666666);
  static const Color errorRed = Color(0xFFE00000);
  static const Color pureBlack = Color(0xFF000000);
  static const Color classicRose = Color(0xFFFBDBE7);

  static MaterialColor primarySwatch = MaterialColor(0xFFE01E69, color);
}

Map<int, Color> color = {
  50: Color.fromRGBO(224, 30, 105, .1),
  100: Color.fromRGBO(224, 30, 105, .2),
  200: Color.fromRGBO(224, 30, 105, .3),
  300: Color.fromRGBO(224, 30, 105, .4),
  400: Color.fromRGBO(224, 30, 105, .5),
  500: Color.fromRGBO(224, 30, 105, .6),
  600: Color.fromRGBO(224, 30, 105, .7),
  700: Color.fromRGBO(224, 30, 105, .8),
  800: Color.fromRGBO(224, 30, 105, .9),
  900: Color.fromRGBO(224, 30, 105, 1),
};

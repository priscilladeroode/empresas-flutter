class AssetsNames {
  AssetsNames._();

  //Images
  static const String logoIcon = 'assets/images/logo_icon.png';
  static const String logoIconAndName = 'assets/images/logo_home.png';
  static const String gradientBackgroundSmall =
      'assets/images/gradient_background_sm.png';
  static const String gradientBackgroundLarge =
      'assets/images/gradient_background_lg.png';
  static const String gradientBackgroudRectangle =
      'assets/images/gradient_background_rectangle.png';

  //Icons
  static const String errorIcon = 'assets/icons/error.svg';
  static const String loadingIcon = 'assets/icons/loading_icon.svg';
  static const String iconsBackground = 'assets/images/icons_background.png';
}

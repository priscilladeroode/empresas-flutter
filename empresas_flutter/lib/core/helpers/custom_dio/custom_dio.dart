import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import 'package:empresas_flutter/core/helpers/custom_dio/dio_interceptor.dart';

class CustomDio extends DioForNative {
  final DioInterceptor dio;
  CustomDio(this.dio, [BaseOptions? options]) : super(options) {
    interceptors.add(dio);
  }
}

import 'package:dio/dio.dart';
import 'package:empresas_flutter/core/presenter/auth_store.dart';
import 'package:empresas_flutter/enterprises/external/utils/enterprise_server_path.dart';

class DioInterceptor extends InterceptorsWrapper {
  final AuthStore authStore;

  DioInterceptor(this.authStore);

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (authStore.userInfo != null && options.path.contains(enterprisePath)) {
      options.headers.addAll({
        "Content-type": "application/json",
        "access-token": authStore.userInfo!.accessToken,
        "client": authStore.userInfo!.client,
        "uid": authStore.userInfo!.uid,
      });
    }

    return super.onRequest(options, handler);
  }

  @override
  Future onError(DioError err, ErrorInterceptorHandler handler) async {
    return super.onError(err, handler);
  }
}

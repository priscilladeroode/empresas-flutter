abstract class Failure implements Exception {
  String? get message;
}

class DatasourceFailure extends Failure {
  final String? message;
  DatasourceFailure({this.message});
}

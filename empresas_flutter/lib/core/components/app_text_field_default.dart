import 'package:empresas_flutter/core/helpers/assets_names.dart';
import 'package:empresas_flutter/core/themes/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppTextField extends StatefulWidget {
  final Function()? onTap;
  final Function(String)? onChanged;
  final String? label;
  final FocusNode? focusNode;
  final String errorMessage;
  final bool isPasswordField;
  final Widget? prefixIcon;
  final String? hintText;

  const AppTextField(
      {Key? key,
      this.label,
      this.onTap,
      this.focusNode,
      this.onChanged,
      this.errorMessage = '',
      this.isPasswordField = false,
      this.prefixIcon,
      this.hintText})
      : super(key: key);

  @override
  _AppTextFieldState createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  late FocusNode focus;
  bool focused = false;
  bool _obscureText = false;

  @override
  void initState() {
    focus = widget.focusNode ?? FocusNode();
    focus.addListener(() {
      focused = focus.hasFocus;
    });
    if (widget.isPasswordField) _obscureText = true;
    super.initState();
  }

  @override
  void dispose() {
    focus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if (widget.label != null)
          Text(
            widget.label!,
            style: Theme.of(context).textTheme.bodyText2!.copyWith(
                  color: focused ? AppColors.ceriseRed : AppColors.doveGrey,
                ),
          ),
        TextFormField(
          cursorColor: AppColors.ceriseRed,
          focusNode: focus,
          onTap: widget.onTap,
          cursorHeight: 26,
          obscureText: _obscureText,
          onChanged: widget.onChanged,
          style: Theme.of(context).textTheme.bodyText1,
          decoration: InputDecoration(
            hintText: widget.hintText,
            prefixIcon: widget.prefixIcon,
            enabledBorder: widget.errorMessage.isNotEmpty
                ? OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.errorRed),
                  )
                : null,
            suffixIcon: widget.errorMessage.isNotEmpty
                ? Padding(
                    padding: const EdgeInsets.all(12),
                    child: SvgPicture.asset(
                      AssetsNames.errorIcon,
                    ),
                  )
                : widget.isPasswordField
                    ? IconButton(
                        icon: _obscureText
                            ? Icon(Icons.visibility)
                            : Icon(Icons.visibility_off),
                        onPressed: () {
                          setState(() => _obscureText = !_obscureText);
                        },
                      )
                    : null,
          ),
        ),
      ],
    );
  }
}

import 'package:empresas_flutter/core/helpers/assets_names.dart';
import 'package:empresas_flutter/core/presenter/auth_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final controller = Modular.get<AuthStore>();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 3)).then((value) {
      Modular.to.navigate('/auth', replaceAll: true);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AssetsNames.gradientBackgroundLarge),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Image.asset(AssetsNames.logoIconAndName),
        ),
      ),
    );
  }
}

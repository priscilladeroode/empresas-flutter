import 'package:empresas_flutter/authentication/domain/entities/logged_user_info.dart';
import 'package:mobx/mobx.dart';

part 'auth_store.g.dart';

class AuthStore = _AuthStoreBase with _$AuthStore;

abstract class _AuthStoreBase with Store {
  @observable
  LoggedUserInfo? userInfo;

  @computed
  bool get isLogged => userInfo != null;

  @action
  void setUser({LoggedUserInfo? loggedUser}) => userInfo = loggedUser;
}

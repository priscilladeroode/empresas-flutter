import 'package:dio/dio.dart';
import 'package:empresas_flutter/authentication/authentication_module.dart';
import 'package:empresas_flutter/core/helpers/custom_dio/custom_dio.dart';
import 'package:empresas_flutter/core/helpers/custom_dio/dio_interceptor.dart';
import 'package:empresas_flutter/core/helpers/server_path.dart';
import 'package:empresas_flutter/core/presenter/auth_store.dart';
import 'package:empresas_flutter/core/presenter/splash_screen.dart';
import 'package:empresas_flutter/enterprises/enterprises_module.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppModule extends Module {
  static List<Bind> exports = [
    Bind((i) => AuthStore()),
    Bind((i) => CustomDio(i(), i())),
    Bind((i) => DioInterceptor(i())),
    Bind.lazySingleton((i) => BaseOptions(
          baseUrl: baseUrl,
        ))
  ];

  @override
  final List<Bind> binds = [...exports];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, __) => SplashScreen()),
    ModuleRoute('/auth', module: AuthenticationModule()),
    ModuleRoute('/enterprises', module: EnterpriseModule()),
  ];
}

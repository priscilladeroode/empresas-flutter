# Empresas Flutter

## Requisitos

Este projeto foi desenvolvido utilizando Flutter SDK[2.2.1] e Dart[2.13.1].


## Getting Started

1. Clone este repositorio para sua mÃ¡quina.
2. Abra o console no root do projeto.
3. Rodar os seguintes comandos em ordem:

```flutter pub get```

```flutter run```

## Bibliotecas

**_dependencies_**

* [flutter_modular](https://pub.dev/packages/flutter_modular)
* [dartz](https://pub.dev/packages/dartz)
* [google_fonts](https://pub.dev/packages/google_fonts)
* [flutter_svg](https://pub.dev/packages/flutter_svg)
* [dio](https://pub.dev/packages/dio)
* [mobx](https://pub.dev/packages/mobx)
* [flutter_mobx](https://pub.dev/packages/flutter_mobx)
* [string_validator](https://pub.dev/packages/string_validator)
* [asuka](https://pub.dev/packages/asuka)
* [equatable](https://pub.dev/packages/equatable)
* [cached_network_image](https://pub.dev/packages/cached_network_image)

**_dev_dependencies_**

* [mobx_codegen](https://pub.dev/packages/mobx_codegen)
* [build_runner](https://pub.dev/packages/build_runner)